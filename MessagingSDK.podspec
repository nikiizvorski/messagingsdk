#
# Be sure to run `pod lib lint MessagingSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MessagingSDK'
  s.version          = '1.4.2'
  s.summary          = 'ViableGrid.io IOS SDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Messaging SDK is the tool that gives developers the possibility to allow users to connect and message each other completely offline.
                       DESC

  s.homepage         = 'https://bitbucket.org/nikiizvorski/messagingsdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'APACHE 2.0', :file => 'LICENSE' }
  s.author           = { 'nikiizvorski' => 'dev@airdates.co' }
  s.source           = { :git => 'https://nikiizvorski@bitbucket.org/nikiizvorski/messagingsdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.ios.deployment_target = '8.0'
  
  s.source_files = 'MessagingSDK/Classes/*.{h,m,swift}'
  
  s.frameworks = 'CoreBluetooth','LocalAuthentication'
  s.vendored_frameworks = 'Messaging.framework', 'GoogleToolboxForMac.framework'
  s.static_framework = true
  
  s.library = 'sqlite3','c++'
  
  # Specify the frameworks we are providing.
  # The app using this Pod should _not_ link these Frameworks,
  # they are bundled as a part of this Pod for technical reasons.
  #  s.vendored_frameworks = 'NearbyConnections.framework'
  
  # LDFLAGS required by Firebase dependencies.
  s.pod_target_xcconfig = {
      'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks',
      'OTHER_LDFLAGS' => '$(inherited) -ObjC'
  }
  
  # s.resource_bundles = {
  #   'MessagingSDK' => ['MessagingSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
