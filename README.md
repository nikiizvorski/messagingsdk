# MessagingSDK

[![CI Status](https://img.shields.io/travis/nikiizvorski/MessagingSDK.svg?style=flat)](https://travis-ci.org/nikiizvorski/MessagingSDK)
[![Version](https://img.shields.io/cocoapods/v/MessagingSDK.svg?style=flat)](https://cocoapods.org/pods/MessagingSDK)
[![License](https://img.shields.io/cocoapods/l/MessagingSDK.svg?style=flat)](https://cocoapods.org/pods/MessagingSDK)
[![Platform](https://img.shields.io/cocoapods/p/MessagingSDK.svg?style=flat)](https://cocoapods.org/pods/MessagingSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MessagingSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MessagingSDK'
```

## Author

nikiizvorski, dev@airdates.co

## License

MessagingSDK is available under the MIT license. See the LICENSE file for more info.
