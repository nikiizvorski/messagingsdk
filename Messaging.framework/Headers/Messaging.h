//
//  Messaging.h
//  Messaging
//
//  Created by Niki Izvorski on 15/08/2018.
//  Copyright © 2018 Niki Izvorski. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreBluetooth;

@interface Messaging : NSObject

/*
    Initialize Base Check
 */
- (void) initialize:(NSString*)appSecret;

/*
    User Search Callback
 */
typedef void (^userSearchCallback)(NSString * endpointId, NSString * endpointName);

/*
 Device Search Callback
 */
typedef void (^deviceSearchCallback)(CBPeripheral * peripheral);

/*
 Device Service Callback
 */
typedef void (^deviceServiceCallback)(NSArray<CBService *> * services);

/*
 Device Characteristics Callback
 */
typedef void (^deviceCharacteristicsCallback)(NSArray<CBCharacteristic *> * characteristics);

/*
 Device Characteristics Value Callback
 */
typedef void (^deviceCharacteristicsValueCallback)(NSData * value);

/*
 Device Characteristics Subscribe Callback
 */
typedef void (^deviceCharacteristicsSubscribeCallback)(BOOL status);

/*
 Device Write Characteristic Callback
 */
typedef void (^deviceCharacteristicsWriteCallback)(BOOL status);

/*
 Device Search Callback
 */
typedef void (^deviceConnectedCallback)(BOOL status);

/*
    User Share Callback
 */
typedef void (^userShareCallback)(NSString * endpointId, NSString * endpointName);

/*
    User Message Received Callback
 */
typedef void (^userMessageCallback)(NSString * endpointId, NSString * message);

/*
 User File Received Callback
 */
typedef void (^userFileCallback)(NSString * endpointId, NSString * file);

/*
    User Share Callback
 */
typedef void (^userAcceptedConnection)(BOOL accepted);

/*
    User Share Callback
 */
typedef void (^userRejectedConnection)(BOOL rejected);

/*
    User Share Callback
 */
typedef void (^userDisconnected)(BOOL disconnected);

/*
    Accept Messaging Connection and results
 */
- (void) acceptConnection:(NSString*)endpointId;

/*
    Request Messaging Connection and results
 */
- (void) requestUserConnection:(NSString*)endpointId endpointName:(NSString*)endpointName userAcceptedConnection:(userAcceptedConnection)userAccepted userRejectedConnection:(userRejectedConnection)userRejected userDisconnected:(userDisconnected)userDisconnected userMessageCallback:(userMessageCallback)userMessageReceived userFileCallback:(userFileCallback)userFileReceived;

/*
    Start Messaging Seach
 */
- (void) startSearching:(NSString*)channel userSearch:(userSearchCallback) callback;

/*
    Start Device BLE Seach
 */
- (void) startLESearching:(NSString*)value getLEDevice:(deviceSearchCallback) callback;

/*
 Start Device BLE Seach
 */
- (void) connectLE:(CBPeripheral*)device deviceConnection:(deviceConnectedCallback) callback;

/*
 Start Device Services Discovery
 */
- (void) discoverLEServices:(CBPeripheral*)device deviceServices:(deviceServiceCallback) callback;

/*
 Start Device Characteristics Discovery
 */
- (void) discoverLECharacteristics:(CBPeripheral*)device deviceService:(CBService*)service deviceCharacteristics:(deviceCharacteristicsCallback) callback;

/*
 Start Device Characteristic Value Read
 */
- (void) readLECharacteristics:(CBPeripheral*)device deviceCharacteristic:(CBCharacteristic*)characteristic characteristicValue:(deviceCharacteristicsValueCallback) callback;

/*
 Start Device Characteristic Value Write
 */
- (void) writeLECharacteristics:(CBPeripheral*)device deviceCharacteristic:(CBCharacteristic*)characteristic value:(NSString*)data writeStatus:(deviceCharacteristicsWriteCallback) callback;

/*
 Start Device Characteristic Value Write
 */
- (void) writeLEByteCharacteristics:(CBPeripheral*)device deviceCharacteristic:(CBCharacteristic*)characteristic value:(NSData*)data writeStatus:(deviceCharacteristicsWriteCallback) callback;

/*
 Start Device Characteristic Subscribe
 */
- (void) subscribeLECharacteristics:(CBPeripheral*)device deviceCharacteristic:(CBCharacteristic*)characteristic characteristicValue:(deviceCharacteristicsSubscribeCallback) callback;

/*
 Start Device Characteristic unsubscribe
 */
- (void) unsubscribeLECharacteristics:(CBPeripheral*)device deviceCharacteristic:(CBCharacteristic*)characteristic characteristicValue:(deviceCharacteristicsSubscribeCallback) callback;

/*
 Start Device Characteristic Value Notification recieve
 */
- (void) recieveCharacteristicUpdateValue:(CBPeripheral*)device deviceCharacteristic:(CBCharacteristic*)characteristic characteristicValue:(deviceCharacteristicsValueCallback) callback;

/*
    Start Messaging Share
 */
- (void) startSharing:(NSString*)name channel:(NSString*)channel userConnection:(userShareCallback) callback userAcceptedConnection:(userAcceptedConnection)userAccepted userRejectedConnection:(userRejectedConnection)userRejected userDisconnected:(userDisconnected)userDisconnected userMessageCallback:(userMessageCallback)userMessageReceived userFileCallback:(userFileCallback)userFileReceived;

/*
    Send Message Bytes
 */
- (void) sendMessage:(NSString*)userId messageContent:(NSString*)message;

/*
    Send File
 */
- (void) sendFile:(NSString*)userId filePath:(NSString*)filePath;

/*
    Disconnect From UserID
 */
- (void) disconnectFrom:(NSString*)userId;

/*
    Stop Sharing
 */
- (void) stopSharing;

/*
    Stop Searching
 */
- (void) stopSearching;

/*
 Stop LE Searching
 */
- (void) stopLESearching;

@end
